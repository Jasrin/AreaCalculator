<?php
include_once'vendor/autoload.php';

use Pondit\Calculator\AreaCalculator\Rectangle;
use Pondit\Calculator\AreaCalculator\Square;
use Pondit\Calculator\AreaCalculator\Triangle;
use Pondit\Calculator\AreaCalculator\Circle;

echo "<h2 style='text-align: center'>Area Calculator</h2>";

$rectangle = new Rectangle(100,200);
echo "Rectangle:- ".$rectangle->rectangle();
echo "<br/>";

$triangle= new Triangle(50,2);
echo "TriangleArea:- ".$triangle->triangleArea();
echo "<br/>";


$square = new Square(5,2);
echo "Square:- ".$square->square();
echo "<br/>";

$circle = new Circle(2,3.14);
echo "Circle:-". $circle->getArea();
