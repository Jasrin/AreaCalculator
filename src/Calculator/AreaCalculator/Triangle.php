<?php
/**
 * Created by PhpStorm.
 * User: DELL
 * Date: 3/28/2018
 * Time: 10:53 AM
 */

namespace Pondit\Calculator\AreaCalculator;


class Triangle
{
    public $base;
    public $height;

    public function __construct($base,$height)
    {
        $this->base = $base;
        $this->height =$height;

    }

    public function triangleArea(){

        $area = $this->base *$this->height/2;
        return $area;

    }

}