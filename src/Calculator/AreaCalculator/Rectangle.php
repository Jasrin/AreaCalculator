<?php
/**
 * Created by PhpStorm.
 * User: DELL
 * Date: 3/28/2018
 * Time: 10:52 AM
 */

namespace Pondit\Calculator\AreaCalculator;


class Rectangle
{
    public $width;
    public $length;

    public function __construct($width,$length)
    {
        $this->width=$width;
        $this->length=$length;

    }
    public function rectangle(){

        $area = $this->width  * $this->length;
        return $area;
    }

}