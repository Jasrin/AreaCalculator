<?php
/**
 * Created by PhpStorm.
 * User: DELL
 * Date: 3/28/2018
 * Time: 10:54 AM
 */

namespace Pondit\Calculator\AreaCalculator;


class Circle
{
    public $radius;
    public $pi;

    public function __construct($radius,$pi)
    {
        $this->radius=$radius;
        $this->pi = $pi;
    }

    public function getArea(){

        $area = $this->pi* $this->radius * $this->radius;
        return $area;
    }

}