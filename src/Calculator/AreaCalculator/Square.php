<?php
/**
 * Created by PhpStorm.
 * User: DELL
 * Date: 3/28/2018
 * Time: 10:53 AM
 */

namespace Pondit\Calculator\AreaCalculator;


class Square
{
    public $number1 ;
    public $number2;

    public function __construct($n1,$n2)
    {
        $this->number1= $n1 ;
        $this->number2= $n2 ;
    }

    public function square(){

        $area =  $this->number1 ** $this->number2;
        return $area;
    }

}